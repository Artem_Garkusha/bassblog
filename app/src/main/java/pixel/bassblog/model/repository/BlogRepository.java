package pixel.bassblog.model.repository;


import pixel.bassblog.model.domain.PostsResponse;
import rx.Observable;

public interface BlogRepository {

    Observable<PostsResponse> getPosts(String nextPageToken, String label);

    Observable<PostsResponse> search(String nextPageToken, String query);
}
