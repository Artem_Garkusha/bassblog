package pixel.bassblog.model.repository.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pixel.bassblog.BuildConfig;
import pixel.bassblog.model.repository.Api;
import pixel.bassblog.model.repository.BlogRepository;
import pixel.bassblog.model.repository.BlogRetrofitRepository;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    @Provides
    @Singleton
    public Api provideApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        return retrofit.create(Api.class);
    }

    @Provides
    @Singleton
    public BlogRepository provideBlogRepository(Api api) {
        return new BlogRetrofitRepository(api);
    }
}
