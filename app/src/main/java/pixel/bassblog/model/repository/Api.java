package pixel.bassblog.model.repository;

import pixel.bassblog.model.domain.PostsResponse;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Retrofit interface for Google Blogger Api
 */
public interface Api {
    //GET https://www.googleapis.com/blogger/v3/blogs/4928216501086861761/posts?fetchBodies=false&fetchImages=true&maxResults=1&
    //labels=deep&
    // fields=items(id%2Cimages%2Clabels%2Ctitle)%2CnextPageToken&key={YOUR_API_KEY}

    String ITEMS = "items(content,id,images,labels,title),nextPageToken";
    String SEARCH_ITEMS = "items(content,images,labels,title),nextPageToken";

    @GET("blogger/v3/blogs/{BlogId}/posts")
    Observable<PostsResponse> posts(
            @Path("BlogId") String id,
            @Query("fetchBodies") boolean fetchBodies,
            @Query("fetchImages") boolean fetchImages,
            @Query("pageToken") String pageToken,
            @Query("fields") String items,
            @Query("labels") String labels,
            @Query("key") String key);


    // API
    //GET https://www.googleapis.com/blogger/v3/blogs/4928216501086861761/posts/search?q=total&fetchBodies=true&key={YOUR_API_KEY}

    @GET("blogger/v3/blogs/{BlogId}/posts/search")
    Observable<PostsResponse> search(
            @Path("BlogId") String id,
            @Query("q") String query,
            @Query("fields") String items,
            @Query("fetchBodies") boolean fetchBodies,
            @Query("key") String key);
}
