package pixel.bassblog.model.repository;

import pixel.bassblog.BuildConfig;
import pixel.bassblog.model.domain.PostsResponse;
import rx.Observable;


public class BlogRetrofitRepository implements BlogRepository {

    private final boolean FETCH_BODIES = true;
    private Api mApi;

    public BlogRetrofitRepository(final Api api) {
        mApi = api;
    }


    @Override
    public Observable<PostsResponse> getPosts(String nextPageToken, String label) {
        return mApi.posts(BuildConfig.BLOG_ID, FETCH_BODIES, true, nextPageToken, Api.ITEMS, label, BuildConfig.API_KEY);
    }

    @Override
    public Observable<PostsResponse> search(String nextPageToken, String query) {
        return mApi.search(BuildConfig.BLOG_ID, query, Api.SEARCH_ITEMS, FETCH_BODIES, BuildConfig.API_KEY);
    }
}
