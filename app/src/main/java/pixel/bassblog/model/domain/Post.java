package pixel.bassblog.model.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class Post implements Parcelable {

    private String mId;
    private String mTitle;
    private String mLabel;
    private String mImageUrl;
    private String mTrackUrl;

    public Post(String title, String label, String image, String trackUrl) {
        this.mTitle = title;
        this.mLabel = label;
        this.mImageUrl = image;
        this.mTrackUrl = trackUrl;
    }

    protected Post(Parcel in) {
        mId = in.readString();
        mTitle = in.readString();
        mLabel = in.readString();
        mImageUrl = in.readString();
        mTrackUrl = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mTitle);
        dest.writeString(mLabel);
        dest.writeString(mImageUrl);
        dest.writeString(mTrackUrl);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Post> CREATOR = new Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel in) {
            return new Post(in);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };

    public String getTitle() {
        return mTitle;
    }

    public String getLabel() {
        return mLabel;
    }

    public String getmTrackUrl() {
        return mTrackUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

}