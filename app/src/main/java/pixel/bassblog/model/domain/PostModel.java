package pixel.bassblog.model.domain;


import java.util.List;

public class PostModel {
    private String mNextPageToken;
    private List<Post> mItems;

    public String getNextPageToken() {
        return mNextPageToken;
    }

    public void setNextPageToken(String mNextPageToken) {
        this.mNextPageToken = mNextPageToken;
    }

    public List<Post> getItems() {
        return mItems;
    }

    public void setItems(List<Post> mItems) {
        this.mItems = mItems;
    }
}
