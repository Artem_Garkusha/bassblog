package pixel.bassblog.core.persistence;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.view.View;

import pixel.bassblog.core.persistence.holder.ComponentHelper;
import pixel.bassblog.core.view.BaseActivity;
import pixel.bassblog.core.view.BaseFragment;


/**
 * Base fragment that abstracts away presenter persistence management
 * Presenter can be found in dagger component for fragment, returned by {@code getComponent()}
 * <p>
 * One thing to do is to implement {@code createComponent()} method to build object graph for fragment
 */
public abstract class ComponentManagerActivity<C extends HasPresenter, V> extends BaseActivity {

    /**
     * Helper object that contains all the logic to manage object graph state
     */
    private ComponentHelper<C, V> mComponentHelper = new ComponentHelper<>();

    /**
     * Factory object, provides instance of object graph for fragment
     */
    private ComponentCreator<C> mCreator = this::createComponent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mComponentHelper.onCreate(savedInstanceState, savedInstanceState, mCreator);
    }


    @Override
    protected void setupViews() {
        super.setupViews();
        mComponentHelper.attachView((V) this);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        mComponentHelper.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mComponentHelper.onDestroyView(this);
        mComponentHelper.onDestroy(this);
    }

    /**
     * Get object graph for the fragment
     *
     * @return object graph
     */
    public C getComponent() {
        return mComponentHelper.getComponent();
    }

    /**
     * Create an instance of object graph for fragment
     *
     * @return instance of object graph
     */
    protected abstract C createComponent();
}
