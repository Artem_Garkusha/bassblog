package pixel.bassblog.core.persistence;


import pixel.bassblog.core.presenter.Presenter;

/**
 * Base interface for dagger components for fragments with presenters
 */
public interface HasPresenter<P extends Presenter> {
    P getPresenter();
}
