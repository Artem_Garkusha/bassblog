package pixel.bassblog;


import javax.inject.Singleton;

import dagger.Component;
import pixel.bassblog.model.repository.BlogRepository;
import pixel.bassblog.model.repository.di.ApiModule;

@Singleton
@Component(modules = {AppModule.class, ApiModule.class})
public interface AppComponent {

    BlogRepository repository();

//    void inject(PostsAdapter activity);

}
