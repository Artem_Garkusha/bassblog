package pixel.bassblog.ui;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pixel.bassblog.BassBlogApplication;
import pixel.bassblog.R;
import pixel.bassblog.core.persistence.ComponentManagerActivity;
import pixel.bassblog.db.BlogTable;
import pixel.bassblog.ui.di.DaggerTestComponent;
import pixel.bassblog.ui.di.TestComponent;
import pixel.bassblog.model.domain.Post;
import pixel.bassblog.ui.presenter.BlogPresenter;


public class MainActivity extends ComponentManagerActivity<TestComponent, BlogView> implements BlogView<List<Post>>, NavigationView.OnNavigationItemSelectedListener, PostsAdapter.AdapterCallBack {

    @Inject
    BlogPresenter mBlogPresenter;

    private PostsAdapter mPostsAdapter;
    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle mToggle;
    private LinearLayoutManager mLayoutManager;
    private RecycleScrollListener mScrollListener;
    private RecyclerView mRecyclerView;
    private EditText mSearch;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getComponent().inject(this);
        setContentView(R.layout.main_layout);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSearch = (EditText) findViewById(R.id.search);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(mToggle);
        mToggle.syncState();

        assert toolbar != null;
        toolbar.setOnClickListener(view -> {
            mSearch.setVisibility(View.VISIBLE);
            mSearch.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(mSearch, InputMethodManager.SHOW_IMPLICIT);
        });

        mSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideSearch();
                mBlogPresenter.search(mSearch.getText().toString());
                return true;
            }
            return false;
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycle_grid);
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mPostsAdapter = new PostsAdapter(this, getApplicationContext(), new ArrayList<>());
        mScrollListener = new RecycleScrollListener();
        mRecyclerView.addOnScrollListener(mScrollListener);
        mRecyclerView.setAdapter(mPostsAdapter);

        setupViews();
    }

    private void hideSearch() {
        mSearch.setVisibility(View.GONE);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mSearch.getWindowToken(), 0);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mRecyclerView.removeOnScrollListener(mScrollListener);
        mDrawer.removeDrawerListener(mToggle);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else if (mSearch.getVisibility() == View.VISIBLE) {
            hideSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected TestComponent createComponent() {
        return DaggerTestComponent
                .builder()
                .appComponent(BassBlogApplication.getAppComponent(this))
                .build();
    }

    @Override
    public void setData(List<Post> data) {
        if (mPostsAdapter != null) {
            mPostsAdapter.updateData(data);
        }
    }

    @Override
    public void showContent() {

    }

    @Override
    public void notifyError(Throwable throwable) {
        Toast.makeText(getApplicationContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
        throw new RuntimeException(throwable);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        mDrawer.closeDrawer(GravityCompat.START);
        switch (item.getItemId()) {
            case R.id.all_mixes:
                mBlogPresenter.updateLabel(null);
                break;
            case R.id.kbps_320:
                mBlogPresenter.updateLabel("320 kbps");
                break;
            case R.id.deep:
                mBlogPresenter.updateLabel("deep");
                break;
            case R.id.hard:
                mBlogPresenter.updateLabel("hard");
                break;
            case R.id.light:
                mBlogPresenter.updateLabel("light");
                break;
            case R.id.liquid:
                mBlogPresenter.updateLabel("liquid");
                break;
            case R.id.neurofunk:
                mBlogPresenter.updateLabel("neurofunk");
                break;
            case R.id.oldschool:
                mBlogPresenter.updateLabel("oldschool");
                break;
            case R.id.raggajungle:
                mBlogPresenter.updateLabel("ragga-jungle");
                break;
        }
        return true;
    }

    @Override
    public void itemClicked(View image, View title, View label, Post item) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(DetailActivity.ITEM, item);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(MainActivity.this,
                new Pair<>(title, getString(R.string.transition_title)),
                new Pair<>(label, getString(R.string.transition_label)),
                new Pair<>(image, getString(R.string.transition_image))
        );
        ActivityCompat.startActivity(MainActivity.this, intent, options.toBundle());
    }

    /**
     * Class responsible for calculation is recyclerViewer reached a bottom
     * also provides infinity scrolling feature.
     */
    private class RecycleScrollListener extends RecyclerView.OnScrollListener {

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int childCount = mLayoutManager.getChildCount();
            int totalItemCount = mLayoutManager.getItemCount();
            int firstVisiblePosition = mLayoutManager.findFirstVisibleItemPosition();

            if ((childCount + firstVisiblePosition) >= totalItemCount) {

                mBlogPresenter.loadNewPage();
            }
        }
    }
}
