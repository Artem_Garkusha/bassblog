package pixel.bassblog.ui.mapper;

import pixel.bassblog.model.domain.PostModel;
import pixel.bassblog.model.domain.PostsResponse;

public interface BlogMapper {
    PostModel map(PostsResponse response);
}
