package pixel.bassblog.ui.mapper;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pixel.bassblog.model.domain.Post;
import pixel.bassblog.model.domain.PostModel;
import pixel.bassblog.model.domain.PostsResponse;

public class BlogMapperImpl implements BlogMapper {

    final Pattern pattern = Pattern.compile("http(.*?)mp3");

    @Override
    public PostModel map(PostsResponse response) {
        PostModel postModel = new PostModel();
        postModel.setNextPageToken(response.getNextPageToken());

        final List<Post> postsList = new ArrayList<>();
        final List<PostsResponse.RawPost> rawPosts = response.getItems();

        for (PostsResponse.RawPost item : rawPosts) {
            String imageUrl = item.images != null ? item.images.get(0).url : null;
            String label = TextUtils.join(", ", item.labels);
            String title = item.title;

            String trackUrl = "";
            Matcher matcher = pattern.matcher(item.content);
            if (matcher.find()) {
                trackUrl = "http".concat(matcher.group(1).concat("mp3"));
            }
            postsList.add(new Post(title, label, imageUrl, trackUrl));
        }


        postModel.setItems(postsList);
        return postModel;
    }
}
