package pixel.bassblog.ui.presenter;

import pixel.bassblog.core.presenter.BasePresenter;
import pixel.bassblog.ui.BlogView;

/**
 * Google blog presenter
 */
public abstract class BlogPresenter extends BasePresenter<BlogView> {

    public abstract void loadNewPage();

    public abstract void updateLabel(String label);

    public abstract void search(final String query);
}
