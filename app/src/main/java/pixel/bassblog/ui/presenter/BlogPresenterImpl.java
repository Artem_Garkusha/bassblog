package pixel.bassblog.ui.presenter;

import android.os.Bundle;
import android.support.annotation.IntDef;
import android.util.Log;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pixel.bassblog.model.domain.Post;
import pixel.bassblog.model.domain.PostModel;
import pixel.bassblog.model.domain.PostsResponse;
import pixel.bassblog.model.repository.BlogRepository;
import pixel.bassblog.ui.BlogView;
import pixel.bassblog.ui.mapper.BlogMapper;
import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

public class BlogPresenterImpl extends BlogPresenter {
    private final BlogRepository mBlogRepository;
    private final BlogMapper mBlogMapper;

    private Subscription mSubscription;
    private final List<Post> mItems;
    private String mNextPageToken;
    private Observable<PostsResponse> mObservable;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({LOADING, IDLE, LOADING_WITH_CLEAR})
    public @interface LoadingState {
    }

    public static final int LOADING = 0;
    public static final int IDLE = 1;
    public static final int LOADING_WITH_CLEAR = 2;

    private int mLoadingState = IDLE;

    @Inject
    public BlogPresenterImpl(final BlogRepository blogRepository, final BlogMapper mapper) {
        mItems = new ArrayList<>();
        mBlogMapper = mapper;
        mBlogRepository = blogRepository;
    }

    @Override
    public void onCreate(Bundle arguments, Bundle savedInstanceState) {
        super.onCreate(arguments, savedInstanceState);
        Timber.d("Presenter onCreate");
        mObservable = mBlogRepository.getPosts(null, null);
        loadPosts(LOADING);
    }

    @Override
    public void search(String query) {
        mNextPageToken = null;
        mObservable = mBlogRepository.search(null, query);
        loadPosts(LOADING_WITH_CLEAR);
    }

    public void updateLabel(String label) {
        mNextPageToken = null;
        mObservable = mBlogRepository.getPosts(null, label);
        loadPosts(LOADING_WITH_CLEAR);
    }

    private void loadPosts(int state) {
        mLoadingState = state;
        mSubscription = mObservable
                .map(mBlogMapper::map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleNext, this::handleError, this::handleComplete);
    }

    private void handleComplete() {
        mLoadingState = IDLE;
    }

    private void handleNext(PostModel model) {
        if (mLoadingState == LOADING_WITH_CLEAR) {
            mItems.clear();
        }
        mItems.addAll(model.getItems());
        mNextPageToken = model.getNextPageToken();
        updateData();
        mLoadingState = IDLE;
    }

    private void updateData() {
        BlogView view = getView();
        if (view != null) {
            Timber.d("items count %d", mItems.size());
            view.setData(mItems);
            view.showContent();
        }
    }

    private void handleError(Throwable throwable) {
        Timber.e(throwable, "BlogPresenterImpl %s", throwable.getMessage());
        mLoadingState = IDLE;
        BlogView view = getView();
        if (view != null) {
            view.notifyError(throwable);
        }
    }

    private void setInitialData() {
        updateData();
    }


    @Override
    public void attachView(BlogView view) {
        super.attachView(view);
        Timber.d("Presenter attachView");
        setInitialData();
    }

    @Override
    public void detachView() {
        super.detachView();
        Timber.d("Presenter detachView");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Timber.d("Presenter onDestroy");
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
    }

    @Override
    public void loadNewPage() {
        if (mLoadingState == IDLE) {
            Timber.d(mNextPageToken);
            mObservable = mBlogRepository.getPosts(mNextPageToken, null);
            loadPosts(LOADING);
        }
    }
}
