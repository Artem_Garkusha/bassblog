package pixel.bassblog.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import pixel.bassblog.R;
import pixel.bassblog.media.MusicService;
import pixel.bassblog.model.domain.Post;


public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String ITEM = "item";
    private Post mItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_layout);
        mItem = getIntent().getParcelableExtra(ITEM);

        final Picasso mPicasso = Picasso.with(getApplicationContext());
        final ImageView imageView = (ImageView) findViewById(R.id.detail_image);
        mPicasso.load(mItem.getImageUrl()).placeholder(R.drawable.placeholder).into(imageView);

        final TextView title = (TextView) findViewById(R.id.detail_title);
        final TextView label = (TextView) findViewById(R.id.detail_label);

        findViewById(R.id.playbutton).setOnClickListener(this);
        findViewById(R.id.pausebutton).setOnClickListener(this);
        findViewById(R.id.skipbutton).setOnClickListener(this);
        findViewById(R.id.rewindbutton).setOnClickListener(this);
        findViewById(R.id.stopbutton).setOnClickListener(this);

        title.setText(mItem.getTitle());
        label.setText(mItem.getLabel());
    }

    @Override
    public void onClick(View target) {
        final Intent i = new Intent(this, MusicService.class);
        switch (target.getId()) {
            case R.id.rewindbutton:
                i.setAction(MusicService.ACTION_REWIND);
                break;
            case R.id.playbutton:
                i.setAction(MusicService.ACTION_URL);
                i.setData(Uri.parse(mItem.getmTrackUrl()));
                break;
            case R.id.pausebutton:
                i.setAction(MusicService.ACTION_PAUSE);
                break;
            case R.id.skipbutton:
                i.setAction(MusicService.ACTION_SKIP);
                break;
            case R.id.stopbutton:
                i.setAction(MusicService.ACTION_STOP);
                break;
        }
        startService(i);
    }
}
