package pixel.bassblog.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import pixel.bassblog.R;
import pixel.bassblog.model.domain.Post;


public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ImageViewHolder> implements View.OnClickListener {

    public interface AdapterCallBack {
        void itemClicked(View image, View title, View description, Post item);
    }

    private final Picasso mPicasso;
    private List<Post> mData;
    private final LayoutInflater mInflater;

    private final AdapterCallBack mCallback;

    public PostsAdapter(final AdapterCallBack callback, final Context context, List<Post> data) {
        mInflater = LayoutInflater.from(context);
        mCallback = callback;
        mData = data;
        mPicasso = Picasso.with(context);
    }

    public void updateData(List<Post> items) {
        mData = items;
        notifyDataSetChanged();
    }

    @Override
    public void onViewRecycled(ImageViewHolder holder) {
        super.onViewRecycled(holder);
        mPicasso.cancelRequest(holder.mImage);
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = mInflater.inflate(R.layout.post_item_layout, parent, false);
        view.setOnClickListener(this);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        Post item = mData.get(position);
        holder.itemView.setTag(item);
        holder.mTitle.setText(item.getTitle());
        holder.mLabel.setText(item.getLabel());
        mPicasso.load(item.getImageUrl()).placeholder(R.drawable.placeholder).error(R.drawable.placeholder).into(holder.mImage);

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void onClick(View v) {
        Object tag = v.getTag();
        if (tag instanceof Post) {
            mCallback.itemClicked(
                    v.findViewById(R.id.adapter_image),
                    v.findViewById(R.id.adapter_title),
                    v.findViewById(R.id.adapter_label),
                    (Post) tag);
        }

    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {
        protected TextView mTitle;
        protected TextView mLabel;
        protected ImageView mImage;

        public ImageViewHolder(final View itemView) {
            super(itemView);
            mTitle = (TextView) itemView.findViewById(R.id.adapter_title);
            mLabel = (TextView) itemView.findViewById(R.id.adapter_label);
            mImage = (ImageView) itemView.findViewById(R.id.adapter_image);
        }
    }
}
