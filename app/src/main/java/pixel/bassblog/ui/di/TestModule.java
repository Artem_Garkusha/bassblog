package pixel.bassblog.ui.di;

import dagger.Module;
import dagger.Provides;
import pixel.bassblog.core.view.PerFragment;
import pixel.bassblog.model.repository.BlogRepository;
import pixel.bassblog.ui.mapper.BlogMapper;
import pixel.bassblog.ui.mapper.BlogMapperImpl;
import pixel.bassblog.ui.presenter.BlogPresenter;
import pixel.bassblog.ui.presenter.BlogPresenterImpl;


@Module
public class TestModule {

    @Provides
    @PerFragment
    public BlogMapper provideBlogMapper() {
        return new BlogMapperImpl();
    }

    @Provides
    @PerFragment
    BlogPresenter provideTestPresenter(BlogRepository blogRepository, BlogMapper mapper) {
        return new BlogPresenterImpl(blogRepository, mapper);
    }
}
