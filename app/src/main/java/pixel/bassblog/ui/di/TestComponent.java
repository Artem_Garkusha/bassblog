package pixel.bassblog.ui.di;


import dagger.Component;
import pixel.bassblog.AppComponent;
import pixel.bassblog.core.persistence.HasPresenter;
import pixel.bassblog.core.view.PerFragment;
import pixel.bassblog.ui.MainActivity;
import pixel.bassblog.ui.presenter.BlogPresenter;

/**
 * Component for weather screen
 */
@Component(
        dependencies = AppComponent.class,
        modules = TestModule.class
)
@PerFragment
public interface TestComponent extends HasPresenter<BlogPresenter> {
    void inject(MainActivity activity);
}
