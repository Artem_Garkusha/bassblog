package pixel.bassblog.ui;


public interface BlogView<D> {
    void setData(D data);

    void showContent();

    void notifyError(Throwable throwable);
}
