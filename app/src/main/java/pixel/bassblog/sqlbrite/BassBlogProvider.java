package pixel.bassblog.sqlbrite;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import pixel.bassblog.model.domain.Post;


public class BassBlogProvider extends ContentProvider {

    private DbOpenHelper mDbHelper;
    private final static String CONTENT_AUTHORITY = "pixel.bassblog";
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private static final int POST = 100;


    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(CONTENT_AUTHORITY, Post.TABLE, POST);
        return matcher;
    }

    @Override
    public boolean onCreate() {
        mDbHelper = new DbOpenHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        final SQLiteDatabase db = mDbHelper.getReadableDatabase();
//        final SelectionBuilder builder = buildSimpleSelection(uri);
//        final Cursor result = builder.where(selection, selectionArgs).query(db, projection, sortOrder);
//        result.setNotificationUri(getContext().getContentResolver(), uri);
//        return result;
        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case POST:
                return Post.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
//        final SQLiteDatabase db = mDbHelper.getWritableDatabase();
//        db.insertOrThrow(getTable(uri), null, values);
//        getContext().getContentResolver().notifyChange(uri, null);
//        return uri;
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
//        final SQLiteDatabase db = mDbHelper.getWritableDatabase();
//        final SelectionBuilder builder = buildSimpleSelection(uri);
//        getContext().getContentResolver().notifyChange(uri, null);
//        return builder.where(selection, selectionArgs).delete(db);
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
//        final SQLiteDatabase db = mDbHelper.getWritableDatabase();
//        final SelectionBuilder builder = buildSimpleSelection(uri);
//        int result = builder.where(selection, selectionArgs).update(db, values);
//        getContext().getContentResolver().notifyChange(uri, null);
//        return result;
        return 0;
    }

    private String getTable(final Uri uri) {
        switch (sUriMatcher.match(uri)) {
            case POST:
                return Post.TABLE;
            default:
                throw new UnsupportedOperationException("Unknown query uri: " + uri);
        }
    }


//    private SelectionBuilder buildSimpleSelection(Uri uri) {
//        final SelectionBuilder builder = new SelectionBuilder();
//        final int match = sUriMatcher.match(uri);
//        switch (match) {
//            case POST:
//                return builder.table(BlogTable.TABLE_NAME);
//            default:
//                throw new UnsupportedOperationException("Unknown uri: " + uri);
//
//        }
//    }

//    @NonNull
//    @Override
//    public ContentProviderResult[] applyBatch(@NonNull ArrayList<ContentProviderOperation> operations)
//            throws OperationApplicationException {
//        final SQLiteDatabase db = mDbHelper.getWritableDatabase();
//        db.beginTransaction();
//        try {
//            final int numOperations = operations.size();
//            final ContentProviderResult[] results = new ContentProviderResult[numOperations];
//            for (int i = 0; i < numOperations; i++) {
//                results[i] = operations.get(i).apply(this, results, i);
//            }
//            db.setTransactionSuccessful();
//            return results;
//        } finally {
//            db.endTransaction();
//        }
//    }
}
