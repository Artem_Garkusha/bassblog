package pixel.bassblog.db;

import android.net.Uri;

public interface BlogTable extends BaseTableSchema {

    String TABLE_NAME = "blog_table";
    String CONTENT_TYPE = "blog_data";

    Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(TABLE_NAME).build();

    String BLOG_ID = "blog_id";
    String TITLE = "title";
    String LABEL = "label";
    String IMAGE_URL = "image_url";
    String TRACK_URL = "track_url";
}
