package pixel.bassblog.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import timber.log.Timber;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "bassblog.db";
    private static final int DATABASE_VERSION = 1;

    DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        StringBuilder sql = new StringBuilder(1024);

        sql.append("CREATE TABLE ").append(BlogTable.TABLE_NAME).append(" (");
        sql.append(BaseColumns._ID).append(" INTEGER PRIMARY KEY AUTOINCREMENT,");
        sql.append(BlogTable.BLOG_ID).append(" INTEGER,");
        sql.append(BlogTable.TITLE).append(" TEXT,");
        sql.append(BlogTable.LABEL).append(" TEXT,");
        sql.append(BlogTable.IMAGE_URL).append(" TEXT,");
        sql.append(BlogTable.TRACK_URL).append(" TEXT,");
        sql.append("UNIQUE (").append(BlogTable.BLOG_ID).append(") ON CONFLICT REPLACE)");

        db.execSQL(sql.toString());
        sql.setLength(0);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Timber.d("Upgrading database from v." + oldVersion + " to v." + newVersion);

        final String tables[] = {BlogTable.TABLE_NAME};
        for (final String table : tables) {
            db.execSQL("DROP TABLE IF EXISTS " + table);
        }
        onCreate(db);
    }
}
