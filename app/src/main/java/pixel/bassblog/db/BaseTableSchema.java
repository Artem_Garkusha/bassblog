package pixel.bassblog.db;


import android.net.Uri;

public interface BaseTableSchema {
    String CONTENT_AUTHORITY = "pixel.bassblog";
    Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
}
